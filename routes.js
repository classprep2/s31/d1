// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (requests/responses).
// The message sent by the client is called "request".]
// The message sent by the server as an answer is called "response".
const http = require("http");

// Creates a variable "port" to store the port number
// 3000, 4000, 8000, 5000 - Usually used for web development.
const port = 4000;
// "request" and "response" is an object, with property and methods.
const server = http.createServer((req, res) => {
	// We will create two endpoint route for "/greeting" and "/homepage" and will return a response upon accessing.
	// The "url" property refers to the url or the link in the browser (endpoint).
	if(req.url == "/greeting"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Hello Again");
	}
	// Mini activity
	// Create another endpoint for the "/homepage" and send a response "This is the homepage".
	else if (req.url == "/homepage") {
	        res.writeHead(200, {"Content-Type": "text/plain"})
	        res.end('This is the homepage')
	}
	else{
		res.writeHead(404, {"Content-Type": "text/plain"});
		res.end("Page not available.");
		console.log("Error 404, user accessing a wrong url or endpoint.");

	}
});

// Uses the "server" and "port" variables created above.
server.listen(port);
 
// When server is running, console will print the message:
console.log(`Server is now accessible at localhost:${port}`);

