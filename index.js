// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (requests/responses).
// The message sent by the client is called "request".]
// The message sent by the server as an answer is called "response".
let http = require("http");
	
	let port = 4000;

// The "http module" has a createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// createServer() is a method of the http object responsible for creating a server using Node.js
http.createServer(function (request, response) {
	// Use to the writeHead() method to:
		//Set a status code for the response. (200 means "OK")
		//Set the content-type of the response. (plain text message)
	// HTTP Response status codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
	response.writeHead(200, {"Content-Type": "text/plain"}); 

	response.end("Hello World");

}).listen(4000); // A port is a virtual point where network connections start and end.
// Each port is associated with specific process or services.
// The server will be assigned to port 4000 via the "listen()" method where the server will listen to any request that are sent to it and will also send the response on via this port.
// 3000, 4000, 8000, 5000 - Usually used for web development.

console.log("Server is running at localhost:4000");

// We will access the server in the browser using the localhost:4000

